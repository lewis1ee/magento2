<?php

namespace Tasks\HobbyDigi\Api\Data;


interface CustomerInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID             = 'entity_id';
    const EMAIL                 = 'email';
    const FIRSTNAME             = 'firstname';
    const LASTNAME              = 'lastname';
    const CREATED_AT            = 'created_at';
    const COUNT                 = 'count';
    /**#@-*/


    /**
     * Get Title
     *
     * @return int|null
     */
    public function getTitle();

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get ID
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get Count
     *
     * @return int|null
     */
    public function getCount();

    /**
     * Set Title
     *
     * @param int $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Set Content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Set Crated At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Set ID
     *
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname);

    /**
     * Set ID
     *
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname);
}
