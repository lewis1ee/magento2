<?php

namespace Tasks\HobbyDigi\Api\Data;


interface PostInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ANSWER_ID             = 'answer_id';
    const CREATED_BY            = 'created_by';
    const CONTENT               = 'content';
    const CREATED_AT            = 'created_at';
    const COUNT                 = 'count';
    /**#@-*/


    /**
     * Get Title
     *
     * @return int|null
     */
    public function getTitle();

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get ID
     *
     * @return string|null
     */
    public function getIp();

    /**
     * Get Count
     *
     * @return int|null
     */
    public function getCount();

    /**
     * Set Title
     *
     * @param int $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Set Content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Set Crated At
     *
     * @param int $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Set ID
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip);
}
