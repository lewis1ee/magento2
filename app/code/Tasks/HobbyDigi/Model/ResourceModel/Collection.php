<?php


namespace Tasks\HobbyDigi\Model\ResourceModel;

use Magento\Framework\DB\Select;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @return Select
     */
    public function getCount()
    {
        $this->_renderFilters();
        $count = $this->getSelect()
            ->columns(['count' => new \Zend_Db_Expr('COUNT(content)')])
            ->group('content');
        return $count;
    }

    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tasks\HobbyDigi\Model\Answer', 'Tasks\HobbyDigi\Model\ResourceModel\Answer');
    }
}
