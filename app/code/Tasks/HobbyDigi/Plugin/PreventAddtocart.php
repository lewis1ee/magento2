<?php
declare(strict_types=1);


namespace Tasks\HobbyDigi\Plugin;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Http\Context;
use Magento\Framework\Message\ManagerInterface;

use Magento\Store\Model\ScopeInterface;

class PreventAddtocart
{

    /**
     * Scope config
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * HTTP Context
     * Customer session is not initialized yet
     *
     * @var Context
     */
    protected $context;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;


    const IS_DISABLE_ADD_TO_CART = 'catalog/frontend/is_disable_add_to_cart';


    public function __construct(ScopeConfigInterface $scopeConfig,
                                Context $context,
                                ManagerInterface $messageManager)
    {
        $this->scopeConfig = $scopeConfig;
        $this->context = $context;
        $this->_messageManager = $messageManager;
    }

    /**
     * @return bool
     */
    public function afterAddProduct()
    {
        $scope = ScopeInterface::SCOPE_STORE;
        if($this->scopeConfig->getValue(self::IS_DISABLE_ADD_TO_CART, $scope))
        {
            $this->_messageManager->addErrorMessage('Sorry, this product cannot be added in cart now.');
            return false;
        }
        return true;
    }
}
