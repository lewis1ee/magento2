<?php

namespace Tasks\HobbyDigi\Block;

use Magento\Catalog\Model\Product;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Element\Template;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Catalog\Model\ProductRepository;

class Background extends Template
{

    /**
     * HTTP Context
     * Customer session is not initialized yet
     *
     * @var Context
     */
    protected $context;

    /**
     * @var ProductRepository
     */
    public $productRepository;

    /**
     * @var Product
     */
    protected $product;
    /**
     * @var DateTime
     */
    protected $date;



    const IS_DISABLE_ADD_TO_CART = 'catalog/frontend/is_disable_add_to_cart';



    public function __construct(Context $context,
                                DateTime $date,
                                Product $product,
                                ProductRepository $productRepository,
                                array $data = [])
    {
        $this->context = $context;
        $this->date = $date;
        $this->product = $product;
        $this->productRepository = $productRepository;
        parent::__construct($context, $data);

    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $product = $this->productRepository->getById($this->getRequest()->getParam('id'));
        $expireDate = $product->getData('is_expired');
        if(!isset($expireDate) || $this->date->gmtDate() < $expireDate)
        {
            return false;
        }
        return true;
    }
}
