<?php


namespace Tasks\HobbyDigi\Block;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Tasks\HobbyDigi\Model\ResourceModel\Collection as AnsCollection;
use \Tasks\HobbyDigi\Model\ResourceModel\CollectionFactory as AnsCollectionFactory;
use \Tasks\HobbyDigi\Model\Answer;

class Survey extends Template
{
    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_ansCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param AnsCollectionFactory $ansCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        AnsCollectionFactory $ansCollectionFactory,
        array $data = []
    ) {
        $this->_ansCollectionFactory = $ansCollectionFactory;
        parent::__construct($context, $data);
    }


    /**
     * @return Answer[]
     */
    public function getAns()
    {
        /** @var AnsCollection $ansCollection */
        $ansCollection = $this->_ansCollectionFactory->create();
        $ansCollection->addFieldToSelect('*')
            ->setOrder('created_at', 'DESC')
            ->setPageSize(10)
            ->load();
        return $ansCollection->getItems();
    }

    public function getCount()
    {
        /** @var AnsCollection $ansCollection */
        $ansCollection = $this->_ansCollectionFactory->create();
        $ansCollection->addFieldToSelect('content')
            ->getCount();
        $ansCollection->setOrder('count', 'DESC')->load();
        return $ansCollection->getItems();
    }
}
