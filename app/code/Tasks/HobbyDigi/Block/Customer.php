<?php


namespace Tasks\HobbyDigi\Block;


use Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\View\Element\Template;
use \Magento\Customer\Model\ResourceModel\Customer as CustomerCollection;
use \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;

class Customer extends Template
{
    /**
     * CollectionFactory
     * @var null|CustomerCollectionFactory
     */
    protected $_customerCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerCollectionFactory $customerCollectionFactory,
        array $data = []
    ) {
        $this->_customerCollectionFactory = $customerCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getCount()
    {
        /** @var CustomerCollectionFactory $CustomerCollectionFactory */
        $cusCollection = $this->_customerCollectionFactory->create();
        return $cusCollection->addFieldToSelect('*')->getSize();
    }
}
