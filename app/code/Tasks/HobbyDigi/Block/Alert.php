<?php


namespace Tasks\HobbyDigi\Block;

use Magento\Framework\View\Element\Template;

class Alert extends Template
{

    public function isShow()
    {
        return $this->_request->getParam('cat');
    }
}
