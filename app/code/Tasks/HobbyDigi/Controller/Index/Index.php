<?php
namespace Tasks\HobbyDigi\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Result\Page;
use \Magento\Framework\Exception\LocalizedException;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;

class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    public $remoteAddress;
    private $setup;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param RemoteAddress $remoteAddress
     * @param ModuleDataSetupInterface $setup
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ModuleDataSetupInterface $setup,
        RemoteAddress $remoteAddress
    ) {
        parent::__construct($context);
        $this->setup = $setup;
        $this->resultPageFactory = $resultPageFactory;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * Prints the blog from informed order id
     * @param ModuleDataSetupInterface $setup
     * @return Page
     * @throws LocalizedException
     */
    public function execute()
    {

        $this->remoteAddress->getRemoteAddress();
        $this->setup->startSetup();
        $table = $this->setup->getTable('tasks_hobbydigi_survey_answer');
        $ans = [
            'Content' => $this->getRequest()->getPost('ans'),
            'created_by' => $this->remoteAddress->getRemoteAddress(),
        ];

        if (!empty($ans['Content'])) {
            $this->setup
                ->getConnection()
                ->insert($table, $ans);
            $this->setup->endSetup();
            $resultPage = $this->resultPageFactory->create();
            $this->messageManager->addSuccessMessage('Your answer have been saved. Thank you.');
            $this->_redirect('/');
            return $resultPage;
        }
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;

    }
}
