<?php
namespace Tasks\HobbyDigi\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Tasks\HobbyDigi\Setup
 *
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableAnswer = $setup->getTable('tasks_hobbydigi_survey_answer');

        if ($setup->getConnection()->isTableExists($tableAnswer) != true)
        {
            $tableA = $setup->getConnection()
                ->newTable($tableAnswer)
                ->addColumn(
                    'answer_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary'  => true,
                    ],
                    'Answer ID'
                )
                ->addColumn(
                    'Content',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ],
                    'Content'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'created_by',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ],
                    'Created by'
                )
                ->setComment('HobbyDigi Survey - Answer');
            $setup->getConnection()->createTable($tableA);
        }

        $setup->endSetup();
    }
}
